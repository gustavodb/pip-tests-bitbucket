from distutils.core import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='pip_tests_bitbucket',
      version='0.1',
      packages=['pip_tests_bitbucket'],
      install_requires=requirements,
     )
